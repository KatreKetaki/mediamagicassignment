package com.mediamagicassignment.listener;

import java.io.InputStream;

public interface AuthorNetworkListener  {

     void getAuthorNetworkResult(InputStream inputStream);
}
