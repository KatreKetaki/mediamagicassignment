package com.mediamagicassignment.listener;

import com.mediamagicassignment.data.AuthorDetails;

import java.util.List;

public interface AuthorDetailListener {

    void getAuthorDetails(List<AuthorDetails> authorDetails);
}
