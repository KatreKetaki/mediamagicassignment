package com.mediamagicassignment.listener;

import com.mediamagicassignment.data.AuthorDetails;

import java.util.List;

public interface AuthorDetailParseListener extends AuthorNetworkListener {

    void getAuthorParsedList(List<AuthorDetails> authorDetails);
}
