package com.mediamagicassignment.listener;

import android.graphics.Bitmap;

public interface AuthorBitmapListener {
    void authorBitmapLoaded(Bitmap bitmap);
}
