package com.mediamagicassignment.networkmanager;

import android.util.Log;

import com.mediamagicassignment.listener.AuthorNetworkListener;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NetworkManager {

    private static final String TAG ="NetworkManager" ;
    private static NetworkManager networkManager;
    private AuthorNetworkListener authorNetworkListener;



    private NetworkManager()
    {
        if(networkManager != null)
        {
            throw new RuntimeException("Use getInstance() method to create instance of NerworkManager");
        }
    }

    public  static NetworkManager getInstance()
    {
        if(networkManager == null)
        {
            networkManager = new NetworkManager();

        }
        return networkManager;
    }

    public void setListener(AuthorNetworkListener authorNetworkListener)
    {
        this.authorNetworkListener = authorNetworkListener;
    }


    private void getNetworkResponse(String url)
    {
        URL webUrl= null;
        try {
            webUrl = new URL(url);

            Log.v(TAG,"Website url:- "+webUrl);

            HttpURLConnection urlConnection= (HttpURLConnection) webUrl.openConnection();
            urlConnection.setReadTimeout(30000);
            urlConnection.setDoInput(true);
            urlConnection.connect();
            urlConnection.setInstanceFollowRedirects(true);
            InputStream in = urlConnection.getInputStream();

            authorNetworkListener.getAuthorNetworkResult(in);

        } catch (MalformedURLException e) {
            Log.e(TAG,""+e.getMessage());
        } catch (IOException e) {
            Log.e(TAG,""+e.getMessage());
        }

    }



    public void executeRequest(final String url)
    {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        executorService.execute(new Runnable(){

            @Override
            public void run() {
                getNetworkResponse(url);
            }
        });
    }


}
