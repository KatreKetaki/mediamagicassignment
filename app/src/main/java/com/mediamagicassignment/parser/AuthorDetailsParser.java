package com.mediamagicassignment.parser;

import android.util.Log;

import com.mediamagicassignment.data.AuthorDetails;
import com.mediamagicassignment.listener.AuthorDetailParseListener;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class AuthorDetailsParser {

    private static final String TAG ="AuthorDetailsParser" ;
    private List<AuthorDetails> authorDetailsList = new ArrayList<>();
    private AuthorDetailParseListener authorDetailParseListener;

    public AuthorDetailsParser(AuthorDetailParseListener authorDetailParseListener) {
        this.authorDetailParseListener = authorDetailParseListener;
    }

    public void parseData(String response)
    {
        JSONArray jsonArray;
        try {
            jsonArray = new JSONArray(response);
            for(int i=0; i<jsonArray.length();i++) {
                AuthorDetails authorDetails = new AuthorDetails();

                authorDetails.setId(jsonArray.getJSONObject(i).optInt("id"));
                authorDetails.setAuthor(jsonArray.getJSONObject(i).optString("author"));
                authorDetailsList.add(authorDetails);

                authorDetailParseListener.getAuthorParsedList(authorDetailsList);

            }

            } catch (JSONException e) {
           Log.e(TAG,""+e.getMessage());
        }
    }

}




