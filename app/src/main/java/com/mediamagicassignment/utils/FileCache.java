package com.mediamagicassignment.utils;

import android.content.Context;

import java.io.File;

public class FileCache {

    private File cacheDir;

    public FileCache(Context context)
    {
        if (android.os.Environment.getExternalStorageDirectory().equals(
                android.os.Environment.MEDIA_MOUNTED))
        {
            cacheDir = new File(android.os.Environment.getExternalStorageDirectory(),"AuthorList");
        }
        else
        {
            cacheDir=context.getCacheDir();
        }

        if(!cacheDir.exists())
        {
            cacheDir.mkdirs();
        }
    }

    File getFile(String url){

        String filename=String.valueOf(url.hashCode());

        return new File(cacheDir, filename);
    }

}