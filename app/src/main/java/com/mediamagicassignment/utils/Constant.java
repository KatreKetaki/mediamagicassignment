package com.mediamagicassignment.utils;

public class Constant {

    public static String BASE_URL="https://picsum.photos";
    public static String IMAGE_AUTHOR_LIST_URL=BASE_URL+"/list";
    public static String IMAGE_URL = BASE_URL+"/300/300?image=";

}
