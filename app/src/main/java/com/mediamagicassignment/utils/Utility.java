package com.mediamagicassignment.utils;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

public class Utility {


    private static final String TAG ="Utility" ;

    public static String inputStreamToString(InputStream in) {
        String rLine;
        StringBuilder result=new StringBuilder();

        InputStreamReader isr=new InputStreamReader(in);
        BufferedReader rd=new BufferedReader(isr);
        try {
            while ((rLine = rd.readLine()) != null) {
                result.append(rLine);
            }
        }catch(IOException e)
        {

            Log.e(TAG,""+e.getMessage());
        }

        Log.v(TAG,"Result String:-  "  + result);

        return  result.toString();
    }

    public static void CopyStream(InputStream is, OutputStream os) {

        final int buffer_size=1024;
        try
        {

            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
                //Read byte from input stream

                int count=is.read(bytes, 0, buffer_size);
                if(count==-1)
                    break;

                //Write byte from output stream
                os.write(bytes, 0, count);
            }
        }
        catch(Exception ignored){}
    }
}
