package com.mediamagicassignment.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.mediamagicassignment.R;
import com.mediamagicassignment.data.AuthorDetails;
import com.mediamagicassignment.utils.Constant;
import com.mediamagicassignment.utils.ImageLoader;
import com.mediamagicassignment.viewmodel.AuthorAdapterViewModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AuthorViewAdapter extends RecyclerView.Adapter<AuthorViewAdapter.ViewHolder> {


    private static final String TAG = "AuthorViewAdapter";
    private final Activity context;
    private List<AuthorDetails> authorDetails;
    private Map<Integer,Bitmap> bitmaps ;

    public AuthorViewAdapter(List<AuthorDetails> authorDetails, Activity context) {
        this.authorDetails=authorDetails;
        this.context = context;
        bitmaps = new HashMap<>();


    }

    @NonNull
    @Override
    public AuthorViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.author_view_item,parent,false);


        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final AuthorViewAdapter.ViewHolder holder, final int position) {

        holder.authorName.setText(authorDetails.get(position).getAuthor());
        int id = authorDetails.get(position).getId();

        Log.v(TAG,"Position :" + position);
        new ImageLoader(context).DisplayImage(Constant.IMAGE_URL+id,holder.imageAuthor);
//        AuthorAdapterViewModel adapterViewModel =
//                new ViewModelProvider((ViewModelStoreOwner) context).get(AuthorAdapterViewModel.class);
//
//        adapterViewModel.loadAuthorImage(id).observe((LifecycleOwner) context,
//                new Observer<Bitmap>() {
//                    @Override
//                    public void onChanged(Bitmap bitmap) {
//                        bitmaps.put(position,bitmap);
//                    }
//                });
//
//        holder.imageAuthor.setImageBitmap(bitmaps.get(position));
    }

    @Override
    public int getItemCount() {
        return authorDetails.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageAuthor;
        TextView authorName;


        ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageAuthor = itemView.findViewById(R.id.img_author);
            authorName=itemView.findViewById(R.id.txt_author);

        }
    }

}
