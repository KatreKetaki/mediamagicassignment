package com.mediamagicassignment.repo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.mediamagicassignment.listener.AuthorBitmapListener;
import com.mediamagicassignment.listener.AuthorNetworkListener;
import com.mediamagicassignment.networkmanager.NetworkManager;
import com.mediamagicassignment.utils.Constant;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class AuthorBitmapDataRepository extends BaseDataRepository implements AuthorNetworkListener
{
    private int authorId;
    private AuthorBitmapListener bitmapListener;

    public AuthorBitmapDataRepository(AuthorBitmapListener bitmapListener, int authorId) {
        this.bitmapListener = bitmapListener;
        this.authorId = authorId;
    }

    @Override
    public void getAuthorData() {
        String url = Constant.IMAGE_URL+authorId;
        NetworkManager networkManager = NetworkManager.getInstance();
        networkManager.executeRequest(url);
        networkManager.setListener(this);
    }

    @Override
    public void getAuthorNetworkResult(InputStream inputStream) {
        Bitmap imageBitmap =  BitmapFactory.decodeStream(inputStream);
        bitmapListener.authorBitmapLoaded(imageBitmap);
    }
}
