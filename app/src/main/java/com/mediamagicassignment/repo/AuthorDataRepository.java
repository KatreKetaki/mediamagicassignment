package com.mediamagicassignment.repo;

import com.mediamagicassignment.data.AuthorDetails;
import com.mediamagicassignment.listener.AuthorDetailListener;
import com.mediamagicassignment.listener.AuthorDetailParseListener;
import com.mediamagicassignment.listener.AuthorNetworkListener;

import com.mediamagicassignment.networkmanager.NetworkManager;
import com.mediamagicassignment.parser.AuthorDetailsParser;

import com.mediamagicassignment.utils.Constant;
import com.mediamagicassignment.utils.Utility;

import java.io.InputStream;
import java.util.List;

public class AuthorDataRepository  extends BaseDataRepository implements  AuthorDetailParseListener, AuthorNetworkListener {

    private AuthorDetailListener authorDetailListener;

    public AuthorDataRepository(AuthorDetailListener authorDetailListener) {
        this.authorDetailListener = authorDetailListener;
    }

    @Override
    public void getAuthorData() {
        NetworkManager networkManager = NetworkManager.getInstance();
        networkManager.executeRequest(Constant.IMAGE_AUTHOR_LIST_URL);
        networkManager.setListener(this);
    }

    @Override
    public void getAuthorParsedList(List<AuthorDetails> authorDetails) {
        authorDetailListener.getAuthorDetails(authorDetails);
    }

    @Override
    public void getAuthorNetworkResult(InputStream inputStream) {

        String result = Utility.inputStreamToString(inputStream);
        AuthorDetailsParser authorDetailsParser = new AuthorDetailsParser(this);
        authorDetailsParser.parseData(result);
    }

}
