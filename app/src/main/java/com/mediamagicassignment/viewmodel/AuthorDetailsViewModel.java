package com.mediamagicassignment.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mediamagicassignment.data.AuthorDetails;
import com.mediamagicassignment.listener.AuthorDetailListener;
import com.mediamagicassignment.repo.AuthorDataRepository;
import com.mediamagicassignment.repo.BaseDataRepository;

import java.util.List;

public class AuthorDetailsViewModel extends ViewModel implements AuthorDetailListener {


    private MutableLiveData<List<AuthorDetails>> authorDetailsLiveData;

    public LiveData<List<AuthorDetails>> fetchAuthorDetailsList() {
        if (authorDetailsLiveData == null) {
            authorDetailsLiveData = new MutableLiveData<>();
            fetchAuthorList();
        }
        return authorDetailsLiveData;
    }


    private void fetchAuthorList()
    {
        BaseDataRepository authorDataRepository = new AuthorDataRepository(this);
        authorDataRepository.getAuthorData();
    }


    @Override
    public void getAuthorDetails(List<AuthorDetails> authorDetails) {
        authorDetailsLiveData.postValue(authorDetails);
    }
}
