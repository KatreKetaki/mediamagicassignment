package com.mediamagicassignment.viewmodel;

import android.graphics.Bitmap;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mediamagicassignment.listener.AuthorBitmapListener;
import com.mediamagicassignment.repo.AuthorBitmapDataRepository;
import com.mediamagicassignment.repo.BaseDataRepository;

public class AuthorAdapterViewModel extends ViewModel implements AuthorBitmapListener {

    private MutableLiveData<Bitmap> bitmapMutableLiveData;
    private int authorId;

    public LiveData<Bitmap> loadAuthorImage(int authorId) {
        this.authorId=authorId;
        if (bitmapMutableLiveData==null) {
            bitmapMutableLiveData = new MutableLiveData<>();
        }
        loadAuthorBitmap();
        return bitmapMutableLiveData;
    }

    private void loadAuthorBitmap() {
        BaseDataRepository dataRepository = new AuthorBitmapDataRepository(this,authorId);
        dataRepository.getAuthorData();
    }

    @Override
    public void authorBitmapLoaded(Bitmap bitmap) {
        bitmapMutableLiveData.postValue(bitmap);
    }
}
