package com.mediamagicassignment.viewmodel;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class AuthorAdapterViewModelFactory implements ViewModelProvider.Factory {

    private static final String TAG = "ViewModelFactory";
    private int authorId;

    public AuthorAdapterViewModelFactory(int authorId) {
        this.authorId = authorId;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new AuthorAdapterViewModel();
    }
}
