package com.mediamagicassignment.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mediamagicassignment.R;
import com.mediamagicassignment.adapter.AuthorViewAdapter;
import com.mediamagicassignment.data.AuthorDetails;
import com.mediamagicassignment.viewmodel.AuthorDetailsViewModel;

import java.util.List;

public class AuthorListActivity extends AppCompatActivity {


    private static final int SPAN_COUNT = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_author_list);

        final RecyclerView recyclerView = findViewById(R.id.recyclerView);
        final ProgressBar progressBar = findViewById(R.id.progress_circular);

        AuthorDetailsViewModel authorDetailsViewModel = new ViewModelProvider(this)
                .get(AuthorDetailsViewModel.class);
        authorDetailsViewModel.fetchAuthorDetailsList().observe(this,
                 new Observer<List<AuthorDetails>>() {
            @Override
            public void onChanged(List<AuthorDetails> authorDetails) {
                AuthorViewAdapter authorViewAdapter = new AuthorViewAdapter(authorDetails,AuthorListActivity.this);
                recyclerView.setLayoutManager(
                        new GridLayoutManager(AuthorListActivity.this,SPAN_COUNT));
                recyclerView.setHasFixedSize(true);
                recyclerView.setAdapter(authorViewAdapter);

                authorViewAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
